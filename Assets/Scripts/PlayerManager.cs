using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerManager : NetworkBehaviour
{
   public GameObject Card1;
   public GameObject Card2;
   public GameObject Card3;
   public GameObject Card4;
   public GameObject Card5;
   public GameObject Card6;
   public GameObject Card7;
   public GameObject Card8;
   public GameObject Card9;
   public GameObject Card10;


   public GameObject PlayerArea;
   public GameObject EnemyArea;
   public GameObject DropZone;

   List<GameObject> cards = new List<GameObject>();

   public override void OnStartClient() {
       base.OnStartClient();

       PlayerArea = GameObject.Find("PlayerArea");
       EnemyArea = GameObject.Find("EnemyArea");
       DropZone = GameObject.Find("DropZone");
   }

    [Server]
   public override void OnStartServer() {
       base.OnStartServer();

       cards.Add(Card1);
       cards.Add(Card2);
       cards.Add(Card3);
       cards.Add(Card4);
       cards.Add(Card5);
       cards.Add(Card6);
       cards.Add(Card7);
       cards.Add(Card8);
       cards.Add(Card9);
       cards.Add(Card10);
       Debug.Log(cards);
   }

    [Command]
   public void CmdDealCards() {
       for (int i = 0; i < 3; i++) {
           GameObject card = Instantiate(cards[Random.Range(0, cards.Count)], new Vector2(0,0), Quaternion.identity);
           NetworkServer.Spawn(card, connectionToClient);
           RpcShowCard(card, "Dealt");
       }
   }

   public void PlayCard(GameObject card) 
       {
           CmdPlayCard(card);
       }

    [Command]
    void CmdPlayCard(GameObject card) {
        {
            RpcShowCard(card, "Played");
        }
    }
   

   [ClientRpc]
   void RpcShowCard(GameObject card, string type) {
       if (type == "Dealt") 
       {
            if (hasAuthority) 
            {
                card.transform.SetParent(PlayerArea.transform, false);
            }
            else 
            {
                card.transform.SetParent(EnemyArea.transform, false);
            }
       }

       else if (type == "Played") 
       {
           card.transform.SetParent(DropZone.transform, false);
       }
   }
}
