using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Card : ScriptableObject {

    public new string name;
    public string description;

    public int manaCost;
    public int health;
    public int attack;

    public void Print ()
    {
        Debug.Log(name + ": " + description + " The card costs: " + manaCost);
    }
}
